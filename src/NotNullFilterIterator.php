<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use FilterIterator;
use Stringable;

/**
 * NotNullFilterIterator class file.
 * 
 * This class is an iterator that filters out null results of an inner iterator.
 * 
 * @author Anastaszor
 * @template T of object
 * @psalm-suppress InvalidTemplateParam
 * @extends \FilterIterator<integer, T, \Iterator<integer, ?T>>
 * @phpstan-ignore-next-line
 */
class NotNullFilterIterator extends FilterIterator implements Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \FilterIterator::accept()
	 */
	public function accept() : bool
	{
		/** @phpstan-ignore-next-line */
		return parent::current() !== null;
	}
	
}
