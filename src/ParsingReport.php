<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

/**
 * ParsingReport class file.
 * 
 * This is a simple implementation of the ParsingReportInterface.
 * 
 * @author Anastaszor
 */
class ParsingReport implements ParsingReportInterface
{
	
	/**
	 * The entries.
	 * 
	 * @var array<integer, ParsingReportEntryInterface>
	 */
	protected array $_entries = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : ParsingReportEntryInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return \current($this->_entries) !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParsingReportInterface::addEntry()
	 */
	public function addEntry(ParsingReportEntryInterface $entry) : ParsingReportInterface
	{
		$this->_entries[] = $entry;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParsingReportInterface::clear()
	 */
	public function clear(?string $classname = null) : int
	{
		if(null === $classname)
		{
			$count = \count($this->_entries);
			$this->_entries = [];
			
			return $count;
		}
		
		$count = 0;
		
		/** @var ParsingReportEntryInterface $entry */
		foreach($this->_entries as $key => $entry)
		{
			if($entry->getClassname() === $classname)
			{
				unset($this->_entries[$key]);
				$count++;
			}
		}
		
		if(0 < $count)
		{
			$this->_entries = \array_values($this->_entries);
		}
		
		return $count;
	}
	
}
