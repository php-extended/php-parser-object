<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use RuntimeException;
use Throwable;

/**
 * ParseException class file.
 * 
 * This exception is thrown when the data given to the parser cannot be
 * interpreted as valid data to be packed into an object.
 * 
 * @author Anastaszor
 */
class ParseException extends RuntimeException implements ParseThrowable
{
	
	/**
	 * The name of the target class.
	 * 
	 * @var class-string
	 */
	protected string $_classname;
	
	/**
	 * The data that failed to be parsed.
	 * 
	 * @var ?string
	 */
	protected ?string $_data;
	
	/**
	 * The offset where this exception occured.
	 * 
	 * @var integer
	 */
	protected int $_offset;
	
	/**
	 * Builds a new ParseException with the given target class name, data and
	 * offset of failed parsing. If a message is provided, it will be appened
	 * to the default message.
	 * 
	 * @param class-string $classname
	 * @param string $data
	 * @param integer $offset
	 * @param string $message
	 * @param integer $code
	 * @param Throwable $previous
	 */
	public function __construct(string $classname, ?string $data, int $offset, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_classname = $classname;
		$this->_data = $data;
		$this->_offset = $offset;
		
		$newmsg = 'Failed to parse "{data}" at {offset} for class {class}';
		$newmsg = \strtr($newmsg, [
			'{data}' => null === $data ? '(null)' : $data,
			'{offset}' => (string) $offset,
			'{class}' => $classname,
		]);
		
		if(null !== $message)
		{
			$newmsg .= ' : '.$message;
		}
		
		if(null === $code)
		{
			$code = -1;
		}
		
		parent::__construct($newmsg, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParseThrowable::getClassname()
	 */
	public function getClassname() : string
	{
		return $this->_classname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParseThrowable::getData()
	 */
	public function getData() : ?string
	{
		return $this->_data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParseThrowable::getOffset()
	 */
	public function getOffset() : int
	{
		return $this->_offset;
	}
	
}
