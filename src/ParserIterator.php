<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use Iterator;
use IteratorIterator;
use Stringable;

/**
 * ParserIterator class file.
 * 
 * This class represents an iterator that iterates over an iterator of strings
 * and returns their parsed form on stream.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends \IteratorIterator<integer, ?T, \Iterator<integer, ?T>>
 * @implements \Iterator<integer, ?T>
 */
class ParserIterator extends IteratorIterator implements Iterator, Stringable
{
	
	/**
	 * The parser that parses the values of the inner iterator.
	 * 
	 * @var ParserInterface<T>
	 */
	protected ParserInterface $_parser;
	
	/**
	 * Whether to use the nullable methods.
	 * 
	 * @var boolean
	 */
	protected bool $_nullable;
	
	/**
	 * Whether to use the try methods.
	 * 
	 * @var boolean
	 */
	protected bool $_tryable;
	
	/**
	 * The report to be filled.
	 * 
	 * @var ?ParsingReportInterface
	 */
	protected ?ParsingReportInterface $_report = null;
	
	/**
	 * The count of objects.
	 * 
	 * @var integer
	 */
	protected int $_count = 0;
	
	/**
	 * The current object, if any.
	 * 
	 * @var ?T
	 */
	protected $_current = null;
	
	/**
	 * Builds a new ParserIterator with the given information.
	 * 
	 * @param ParserInterface<T> $parser
	 * @param Iterator<integer|string, null|string|T> $iterator
	 * @param boolean $nullable
	 * @param boolean $tryable
	 * @param ?ParsingReportInterface $report
	 */
	public function __construct(
		ParserInterface $parser,
		Iterator $iterator,
		bool $nullable = false,
		bool $tryable = false,
		?ParsingReportInterface $report = null
	) {
		$this->_parser = $parser;
		$this->_nullable = $nullable;
		$this->_tryable = $tryable;
		$this->_report = $report;
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
		parent::__construct($iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::current()
	 * @return ?T
	 */
	public function current() : ?object
	{
		return $this->_current;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::next()
	 * @throws ParseThrowable
	 */
	public function next() : void
	{
		parent::next();
		$this->_count++;
		$this->_current = $this->getCurrentObject();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::key()
	 */
	public function key() : int
	{
		return $this->_count;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::rewind()
	 * @throws ParseThrowable
	 */
	public function rewind() : void
	{
		parent::rewind();
		$this->_count = 0;
		$this->_current = $this->getCurrentObject();
	}
	
	/**
	 * Gets the current value as parsed object.
	 * 
	 * @return ?T
	 * @throws ParseThrowable
	 */
	protected function getCurrentObject() : ?object
	{
		if(!parent::valid())
		{
			return null;
		}
		
		/** @var null|string|T $parentcurrent */
		$parentcurrent = parent::current();
		if(\is_object($parentcurrent))
		{
			return $parentcurrent;
		}
		
		if(!$this->_tryable)
		{
			return $this->_nullable
				? $this->_parser->parseNullable($parentcurrent)
				: $this->_parser->parse($parentcurrent);
		}
		
		return $this->_nullable
			? $this->_parser->tryParseNullable($parentcurrent, $this->_report, $this->_count)
			: $this->_parser->tryParse($parentcurrent, $this->_report, $this->_count);
	}
	
}
