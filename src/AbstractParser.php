<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use Iterator;
use ReflectionClass;

/**
 * AbstractParser class file.
 * 
 * This class represents a generic parser that must be extended.
 * 
 * @author Anastaszor
 * @template T of object
 * @implements ParserInterface<T>
 */
abstract class AbstractParser implements ParserInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parseNullable()
	 */
	public function parseNullable(?string $data) : ?object
	{
		if(null === $data)
		{
			return null;
		}
		
		if(\trim($data) === '')
		{
			return null;
		}
		
		return $this->parse($data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::tryParse()
	 */
	public function tryParse(?string $data, ?ParsingReportInterface $report = null, int $idx = 0) : ?object
	{
		try
		{
			return $this->parse($data);
		}
		catch(ParseThrowable $exc)
		{
			if(null !== $report)
			{
				$report->addEntry(new ParsingReportEntry(
					$idx,
					$exc->getOffset(),
					$exc->getData(),
					$exc->getClassname(),
					$exc->getMessage(),
				));
			}
			
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::tryParseNullable()
	 */
	public function tryParseNullable(?string $data, ?ParsingReportInterface $report = null, int $idx = 0) : ?object
	{
		try
		{
			return $this->parseNullable($data);
		}
		catch(ParseThrowable $exc)
		{
			if(null !== $report)
			{
				$report->addEntry(new ParsingReportEntry(
					$idx,
					$exc->getOffset(),
					$exc->getData(),
					$exc->getClassname(),
					$exc->getMessage(),
				));
			}
			
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parseAll()
	 */
	public function parseAll(array $datas) : array
	{
		$parsed = [];
		
		foreach($datas as $datum)
		{
			$parsed[] = $this->parse($datum);
		}
		
		return $parsed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parseAllNullable()
	 */
	public function parseAllNullable(array $datas) : array
	{
		$parsed = [];
		
		foreach($datas as $datum)
		{
			$dparsed = $this->parseNullable($datum);
			if(null !== $dparsed)
			{
				$parsed[] = $dparsed;
			}
		}
		
		return $parsed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::tryParseAll()
	 */
	public function tryParseAll(array $datas, ?ParsingReportInterface $report = null) : array
	{
		$parsed = [];
		$idx = 0;
		
		foreach($datas as $datum)
		{
			$tparsed = $this->tryParse($datum, $report, $idx);
			if(null !== $tparsed)
			{
				$parsed[] = $tparsed;
			}
			$idx++;
		}
		
		return $parsed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::tryParseAllNullable()
	 */
	public function tryParseAllNullable(array $datas, ?ParsingReportInterface $report = null) : array
	{
		$parsed = [];
		$idx = 0;
		
		foreach($datas as $datum)
		{
			$tparsed = $this->tryParseNullable($datum, $report, $idx);
			if(null !== $tparsed)
			{
				$parsed[] = $tparsed;
			}
			$idx++;
		}
		
		return $parsed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parseIterator()
	 * @psalm-suppress InvalidReturnType
	 */
	public function parseIterator(Iterator $datas) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ParserIterator($this, $datas, false, false, null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parseIteratorNullable()
	 * @psalm-suppress InvalidReturnType
	 */
	public function parseIteratorNullable(Iterator $datas) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ParserIterator($this, $datas, true, false, null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::tryParseIterator()
	 * @psalm-suppress InvalidReturnType
	 */
	public function tryParseIterator(Iterator $datas, ?ParsingReportInterface $report = null) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ParserIterator($this, $datas, false, true, $report));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::tryParseIteratorNullable()
	 * @psalm-suppress InvalidReturnType
	 */
	public function tryParseIteratorNullable(Iterator $datas, ?ParsingReportInterface $report = null) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ParserIterator($this, $datas, true, true, $report));
	}
	
	/**
	 * Gets the names of the tokens from their constant values.
	 *
	 * @param array<integer, integer> $tokenValues
	 * @return string
	 */
	public function getTokenNameList(array $tokenValues) : string
	{
		$names = [];
		
		foreach($tokenValues as $tokenValue)
		{
			$names[] = $this->getTokenName($tokenValue);
		}
		
		return \implode(', ', $names);
	}
	
	/**
	 * Gets the name of the token from its constant value.
	 *
	 * @param ?integer $tokenValue
	 * @return string
	 */
	public function getTokenName(?int $tokenValue) : string
	{
		$rclass = new ReflectionClass($this);
		
		foreach($rclass->getConstants() as $name => $value)
		{
			if($value === $tokenValue)
			{
				return $name;
			}
		}
		
		return 'T_UNKNOWN';
	}
	
}
