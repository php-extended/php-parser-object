<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

/**
 * ParsingReportEntry class file.
 * 
 * This is a simple implementation of the ParsingReportEntryInterface.
 * 
 * @author Anastaszor
 */
class ParsingReportEntry implements ParsingReportEntryInterface
{
	
	/**
	 * The index where the parsing was in any upper iteration.
	 * 
	 * @var integer
	 */
	protected int $_index;
	
	/**
	 * The offset where the parsing failed.
	 * 
	 * @var integer
	 */
	protected int $_offset;
	
	/**
	 * The data that failed parsing.
	 * 
	 * @var ?string
	 */
	protected ?string $_data;
	
	/**
	 * The class name that failed parsing.
	 * 
	 * @var class-string
	 */
	protected string $_classname;
	
	/**
	 * The message of the error.
	 * 
	 * @var string
	 */
	protected string $_message;
	
	/**
	 * Builds a new ParsingReportEntry with its components.
	 * 
	 * @param integer $index
	 * @param integer $offset
	 * @param string $data
	 * @param class-string $classname
	 * @param string $message
	 */
	public function __construct(int $index, int $offset, ?string $data, string $classname, string $message)
	{
		$this->_index = $index;
		$this->_offset = $offset;
		$this->_data = $data;
		$this->_classname = $classname;
		$this->_message = $message;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParsingReportEntryInterface::getIndex()
	 */
	public function getIndex() : int
	{
		return $this->_index;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParsingReportEntryInterface::getOffset()
	 */
	public function getOffset() : int
	{
		return $this->_offset;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParsingReportEntryInterface::getData()
	 */
	public function getData() : ?string
	{
		return $this->_data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParsingReportEntryInterface::getClassname()
	 */
	public function getClassname() : string
	{
		return $this->_classname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParsingReportEntryInterface::getMessage()
	 */
	public function getMessage() : string
	{
		return $this->_message;
	}
	
}
