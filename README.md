# php-extended/php-parser-object

An implementation of the php-extended/php-parser-interface library.

![coverage](https://gitlab.com/php-extended/php-parser-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-parser-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-parser-object ^8`


## Basic Usage

This library provide a single class, the `AbstractParser` which is
made to be implemented. Its `parse` method must return a single object from
the whole string that its given, and throw a `ParseException` if it is
impossibe to do so.


## License

MIT (See [license file](LICENSE)).
