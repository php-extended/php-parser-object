<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\ParsingReport;
use PhpExtended\Parser\ParsingReportEntry;
use PhpExtended\Parser\ParsingReportEntryInterface;
use PHPUnit\Framework\TestCase;

/**
 * ParsingReportTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\ParsingReport
 * @internal
 * @small
 */
class ParsingReportTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ParsingReport
	 */
	protected ParsingReport $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testCount() : void
	{
		$this->assertEquals(2, $this->_object->count());
	}
	
	public function testIterate() : void
	{
		foreach($this->_object as $key => $entry)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(ParsingReportEntryInterface::class, $entry);
		}
	}
	
	public function testClearAll() : void
	{
		$this->assertEquals(2, $this->_object->clear());
		$this->assertEquals(new ParsingReport(), $this->_object);
	}
	
	public function testClearOne() : void
	{
		$expected = new ParsingReport();
		$expected->addEntry(new ParsingReportEntry(4, 12, 'data2', ParsingReportEntry::class, 'message2'));
		$this->assertEquals(1, $this->_object->clear(ParsingReportTest::class));
		$this->assertEquals($expected, $this->_object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ParsingReport();
		$this->_object->addEntry(new ParsingReportEntry(3, 10, 'data1', ParsingReportTest::class, 'message1'));
		$this->_object->addEntry(new ParsingReportEntry(4, 12, 'data2', ParsingReportEntry::class, 'message2'));
	}
	
}
