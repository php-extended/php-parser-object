<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * ParseExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\ParseException
 *
 * @internal
 *
 * @small
 */
class ParseExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ParseException
	 */
	protected ParseException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetClassname() : void
	{
		$this->assertEquals('class', $this->_object->getClassname());
	}
	
	public function testGetOffset() : void
	{
		$this->assertEquals(1, $this->_object->getOffset());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals('data', $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ParseException('class', 'data', 1, 'message');
	}
	
}
