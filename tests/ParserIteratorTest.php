<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParserIterator;
use PHPUnit\Framework\TestCase;

/**
 * ParserIteratorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\ParserIterator
 *
 * @internal
 *
 * @small
 */
class ParserIteratorTest extends TestCase
{
	
	/**
	 * The iterator to test.
	 * 
	 * @var ParserIterator
	 */
	protected ParserIterator $_iterator;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_iterator).'@'.\spl_object_hash($this->_iterator), $this->_iterator->__toString());
	}
	
	public function testParsed() : void
	{
		$count = 0;
		
		foreach($this->_iterator as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertEquals(new stdClass(), $value);
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$mock = $this->getMockForAbstractClass(AbstractParser::class);
		
		$mock->expects($this->any())
			->method('parse')
			->willReturn(new stdClass())
		;
		
		$this->_iterator = new ParserIterator($mock, new ArrayIterator(['']), false, false, null);
	}
	
}
