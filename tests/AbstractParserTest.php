<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Parser\ParsingReport;
use PHPUnit\Framework\TestCase;

/**
 * AbstractParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\AbstractParser
 *
 * @internal
 *
 * @small
 */
class AbstractParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var AbstractParser
	 */
	protected AbstractParser $_parser;
	
	public function testToString() : void
	{
		$object = $this->_parser;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testParseAllEmpty() : void
	{
		$this->assertEquals([], $this->_parser->parseAll([]));
	}
	
	public function testParseNotEmpty() : void
	{
		$this->assertEquals([new stdClass()], $this->_parser->parseAll(['']));
	}
	
	public function testGetTokenNameFound() : void
	{
		$this->assertEquals('T_TOKEN', $this->_parser->getTokenName(123));
	}
	
	public function testGetTokenNameNotFound() : void
	{
		$this->assertEquals('T_UNKNOWN', $this->_parser->getTokenName(2345));
	}
	
	public function testGetTokenNameList() : void
	{
		$this->assertEquals('T_TOKEN', $this->_parser->getTokenNameList([123]));
	}
	
	public function testParseNullableNull() : void
	{
		$this->assertNull($this->_parser->parseNullable(null));
	}
	
	public function testParseNullableEmpty() : void
	{
		$this->assertNull($this->_parser->parseNullable(''));
	}
	
	public function testParseNullableSuccess() : void
	{
		$this->assertEquals(new stdClass(), $this->_parser->parseNullable('success'));
	}
	
	public function testTryParseCatch() : void
	{
		$this->assertNull($this->_parser->tryParse('error'));
	}
	
	public function testTryParseNullable() : void
	{
		$this->assertNull($this->_parser->tryParseNullable('error'));
	}
	
	public function testParseAllNullable() : void
	{
		$this->assertEquals([new stdClass()], $this->_parser->parseAllNullable(['success']));
	}
	
	public function testTryParseAll() : void
	{
		$report = new ParsingReport();
		$this->assertEquals([new stdClass()], $this->_parser->tryParseAll(['success', 'error'], $report));
		$this->assertEquals(1, $report->count());
	}
	
	public function testTryParseAllNullable() : void
	{
		$report = new ParsingReport();
		$this->assertEquals([new stdClass()], $this->_parser->tryParseAllNullable(['success', 'error'], $report));
		$this->assertEquals(1, $report->count());
	}
	
	public function testParseIterator() : void
	{
		$this->assertEquals([new stdClass()], \iterator_to_array($this->_parser->parseIterator(new ArrayIterator(['success']))));
	}
	
	public function testParseIteratorNullable() : void
	{
		$this->assertEquals([new stdClass()], \iterator_to_array($this->_parser->parseIteratorNullable(new ArrayIterator(['success', null]))));
	}
	
	public function testTryParseIterator() : void
	{
		$this->assertEquals([new stdClass()], \iterator_to_array($this->_parser->tryParseIterator(new ArrayIterator(['success', 'error']))));
	}
	
	public function testTryParseIteratorNullable() : void
	{
		$this->assertEquals([new stdClass()], \iterator_to_array($this->_parser->tryParseIteratorNullable(new ArrayIterator(['success', null, 'error']))));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new class() extends AbstractParser
		{
			
			public const T_TOKEN = 123;
			
			public function parse(?string $data) : object
			{
				if('error' === $data)
				{
					throw new ParseException(AbstractParser::class, $data, 0);
				}
				
				return new stdClass();
			}
			
		};
	}
	
}
