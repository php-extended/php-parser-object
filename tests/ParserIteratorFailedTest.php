<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Parser\ParserIterator;
use PHPUnit\Framework\TestCase;

/**
 * ParserIteratorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\ParserIterator
 *
 * @internal
 *
 * @small
 */
class ParserIteratorFailedTest extends TestCase
{
	
	/**
	 * The iterator to test.
	 * 
	 * @var ParserIterator
	 */
	protected ParserIterator $_iterator;
	
	public function testToString() : void
	{
		$object = $this->_iterator;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testParsed() : void
	{
		$this->expectException(ParseException::class);
		
		$count = 0;
		
		foreach($this->_iterator as $key => $value)
		{
			$count++;
			unset($key, $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$mock = $this->getMockForAbstractClass(AbstractParser::class);
		
		$mock->expects($this->any())
			->method('parse')
			->willThrowException(new ParseException(stdClass::class, '', 0))
		;
		
		
		$this->_iterator = new ParserIterator($mock, new ArrayIterator(['']), false, false, null);
	}
	
}
