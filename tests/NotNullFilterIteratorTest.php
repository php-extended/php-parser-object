<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\NotNullFilterIterator;
use PHPUnit\Framework\TestCase;

/**
 * NotNullFilterIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\NotNullFilterIterator
 * @internal
 * @small
 */
class NotNullFilterIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var NotNullFilterIterator
	 */
	protected NotNullFilterIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$count = 0;
		
		foreach($this->_object as $value)
		{
			$this->assertIsString($value);
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new NotNullFilterIterator(new ArrayIterator(['', null]));
	}
	
}
