<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Parser\ParsingReportEntry;
use PHPUnit\Framework\TestCase;

/**
 * ParsingReportEntryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\ParsingReportEntry
 * @internal
 * @small
 */
class ParsingReportEntryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ParsingReportEntry
	 */
	protected ParsingReportEntry $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIndex() : void
	{
		$this->assertEquals(3, $this->_object->getIndex());
	}
	
	public function testGetOffset() : void
	{
		$this->assertEquals(10, $this->_object->getOffset());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals('data', $this->_object->getData());
	}
	
	public function testGetClassname() : void
	{
		$this->assertEquals(ParsingReportEntryTest::class, $this->_object->getClassname());
	}
	
	public function testGetMessage() : void
	{
		$this->assertEquals('message', $this->_object->getMessage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ParsingReportEntry(3, 10, 'data', ParsingReportEntryTest::class, 'message');
	}
	
}
